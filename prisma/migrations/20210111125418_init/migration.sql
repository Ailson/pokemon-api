/*
  Warnings:

  - The migration will change the primary key for the `Pokemon` table. If it partially fails, the table could be left without primary key constraint.
  - Added the required column `pokemonId` to the `Pokemon` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Pokemon" DROP CONSTRAINT "Pokemon_pkey",
ADD COLUMN     "pokemonId" INTEGER NOT NULL,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD PRIMARY KEY ("id");
