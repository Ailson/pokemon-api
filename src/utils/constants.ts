import { StatName } from "./types";

export const MapStatName: StatName = {
  attack: 'ATTACK',
  defense: 'DEFENSE',
  hp: 'HP',
  'special-attack': 'SPECIAL_ATTACK',
  'special-defense': 'SPECIAL_DEFENSE',
  speed: 'SPEED'
}