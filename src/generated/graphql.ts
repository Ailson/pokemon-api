import { GraphQLResolveInfo } from 'graphql';
export type Maybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type StatName = 
  | 'HP'
  | 'ATTACK'
  | 'DEFENSE'
  | 'SPECIAL_ATTACK'
  | 'SPECIAL_DEFENSE'
  | 'SPEED';

export type Ability = {
  name: Scalars['String'];
};

export type Stat = {
  name: StatName;
  value: Scalars['Int'];
};

export type Type = {
  name: Scalars['String'];
};

export type Pokemon = {
  id: Scalars['Int'];
  name: Scalars['String'];
  height: Scalars['Float'];
  weight: Scalars['Float'];
  image: Scalars['String'];
  type: Array<Maybe<Type>>;
  abilities: Array<Maybe<Ability>>;
  stats: Array<Maybe<Stat>>;
  wasCatched: Scalars['Boolean'];
  user?: Maybe<User>;
};

export type User = {
  name: Scalars['String'];
  email: Scalars['String'];
  pokemons?: Maybe<Array<Maybe<Pokemon>>>;
};

export type UserInput = {
  name: Scalars['String'];
  email: Scalars['String'];
};

export type SavePokemonInput = {
  pokemonId: Scalars['Int'];
  name: Scalars['String'];
};

export type Query = {
  pokemons: Array<Maybe<Pokemon>>;
  user?: Maybe<User>;
};


export type QueryPokemonsArgs = {
  offset?: Maybe<Scalars['Int']>;
};

export type Mutation = {
  createUser: User;
  savePokemon: Scalars['Boolean'];
  removePokemon: Scalars['Boolean'];
};


export type MutationCreateUserArgs = {
  user: UserInput;
};


export type MutationSavePokemonArgs = {
  pokemon: SavePokemonInput;
};


export type MutationRemovePokemonArgs = {
  pokemonId: Scalars['Int'];
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  StatName: ResolverTypeWrapper<Partial<StatName>>;
  Ability: ResolverTypeWrapper<Partial<Ability>>;
  String: ResolverTypeWrapper<Partial<Scalars['String']>>;
  Stat: ResolverTypeWrapper<Partial<Stat>>;
  Int: ResolverTypeWrapper<Partial<Scalars['Int']>>;
  Type: ResolverTypeWrapper<Partial<Type>>;
  Pokemon: ResolverTypeWrapper<Partial<Pokemon>>;
  Float: ResolverTypeWrapper<Partial<Scalars['Float']>>;
  Boolean: ResolverTypeWrapper<Partial<Scalars['Boolean']>>;
  User: ResolverTypeWrapper<Partial<User>>;
  UserInput: ResolverTypeWrapper<Partial<UserInput>>;
  SavePokemonInput: ResolverTypeWrapper<Partial<SavePokemonInput>>;
  Query: ResolverTypeWrapper<{}>;
  Mutation: ResolverTypeWrapper<{}>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Ability: Partial<Ability>;
  String: Partial<Scalars['String']>;
  Stat: Partial<Stat>;
  Int: Partial<Scalars['Int']>;
  Type: Partial<Type>;
  Pokemon: Partial<Pokemon>;
  Float: Partial<Scalars['Float']>;
  Boolean: Partial<Scalars['Boolean']>;
  User: Partial<User>;
  UserInput: Partial<UserInput>;
  SavePokemonInput: Partial<SavePokemonInput>;
  Query: {};
  Mutation: {};
};

export type AbilityResolvers<ContextType = any, ParentType extends ResolversParentTypes['Ability'] = ResolversParentTypes['Ability']> = {
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type StatResolvers<ContextType = any, ParentType extends ResolversParentTypes['Stat'] = ResolversParentTypes['Stat']> = {
  name?: Resolver<ResolversTypes['StatName'], ParentType, ContextType>;
  value?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type TypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['Type'] = ResolversParentTypes['Type']> = {
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type PokemonResolvers<ContextType = any, ParentType extends ResolversParentTypes['Pokemon'] = ResolversParentTypes['Pokemon']> = {
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  height?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  weight?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  image?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  type?: Resolver<Array<Maybe<ResolversTypes['Type']>>, ParentType, ContextType>;
  abilities?: Resolver<Array<Maybe<ResolversTypes['Ability']>>, ParentType, ContextType>;
  stats?: Resolver<Array<Maybe<ResolversTypes['Stat']>>, ParentType, ContextType>;
  wasCatched?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type UserResolvers<ContextType = any, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = {
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  email?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  pokemons?: Resolver<Maybe<Array<Maybe<ResolversTypes['Pokemon']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  pokemons?: Resolver<Array<Maybe<ResolversTypes['Pokemon']>>, ParentType, ContextType, RequireFields<QueryPokemonsArgs, never>>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType>;
};

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  createUser?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationCreateUserArgs, 'user'>>;
  savePokemon?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationSavePokemonArgs, 'pokemon'>>;
  removePokemon?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationRemovePokemonArgs, 'pokemonId'>>;
};

export type Resolvers<ContextType = any> = {
  Ability?: AbilityResolvers<ContextType>;
  Stat?: StatResolvers<ContextType>;
  Type?: TypeResolvers<ContextType>;
  Pokemon?: PokemonResolvers<ContextType>;
  User?: UserResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
