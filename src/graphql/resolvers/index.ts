import Mutation from "./Mutation";
import Pokemon from "./Pokemon";
import Query from "./Query";
import User from "./User";

const resolvers: any = {
  Query,
  Mutation,
  Pokemon,
  User
}

export default resolvers;