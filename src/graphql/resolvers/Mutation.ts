import { MutationResolvers } from "@generated/graphql";
import { Pokemon, User } from "@models/index";
import { Context } from "@utils/types";

const Mutation: MutationResolvers = {
  createUser: async (_, { user }, { uid, prisma }: Context) => {
    const userModel = User({ uid, prisma, ...user });
    return await userModel.createUser();
  },
  savePokemon: async (_, { pokemon }, { uid, prisma }: Context) => {
    const pokemonModel = Pokemon({ uid, prisma, ...pokemon });
    const wasCreated = await pokemonModel.createPokemon();
    return !!wasCreated;
  },
  removePokemon: async (_, { pokemonId }, { uid, prisma }: Context) => {
    const pokemonModel = await Pokemon({ uid, prisma, pokemonId });
    const pokemon = await pokemonModel.getPokemonByUser();
    if (!pokemon) return false;
    const wasDeleted = await pokemonModel.removePokemonByUser({ id: pokemon.id });
    return !!wasDeleted;
  }
}

export default Mutation;