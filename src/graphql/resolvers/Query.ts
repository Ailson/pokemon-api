import { QueryResolvers } from "@generated/graphql";
import { User } from "@models/index";
import { Context } from "@utils/types";

const Query: QueryResolvers = {
  pokemons: async (_, { offset = 0 }, { dataSources }: Context) => {
    const { results } = await dataSources.pokemonAPI.listPokemons(offset);
    const pokemons = await Promise.all(results.map(pokemon => dataSources.pokemonAPI.getPokemonDetail(pokemon.name)))

    return pokemons;
  },
  user: async (_, __, { uid, prisma }: Context) => {
    const userModel = User({ uid, prisma })
    return await userModel.getUser();
  }
}

export default Query;