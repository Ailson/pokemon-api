import { UserModel } from "@utils/types";

export const User = ({ prisma, uid, name, email }: UserModel) => {
  const getUser = async () => await prisma.user.findUnique({ where: { id: uid } });

  const createUser = async () => await prisma.user.create({ data: { id: uid, name, email } });

  return {
    getUser,
    createUser
  }
}