import * as admin from 'firebase-admin';
import Auth from '../auth.interface';

const app = admin.initializeApp({ credential: admin.credential.applicationDefault() });

const FirebaseAuth: Auth = {
  validateIdToken: async (token): Promise<{ uid?: string; }> => {
    try {
      return await app.auth().verifyIdToken(token).then(decodedToken => ({ uid: decodedToken.uid }));
    } catch (error) {
      return { uid: undefined };
    }
  }
}

export default FirebaseAuth;