export default interface Auth {
  validateIdToken(token: string): Promise<{ uid?: string }>;
}