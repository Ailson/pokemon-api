# Pokemon-api 🐷

Project made with Apollo and Prisma to learn how to make an backend from scratch.

## Installation

- Clone the repo
- Run `yarn`
- Create an account in the [Firebase](https://firebase.google.com/docs/admin/setup) and add SDK Admin
- Add the [authentication](https://firebase.google.com/docs/auth) module and enable the email/password method (create an user if you wanna to teste the application)
- If you has docker in your machine, run `docker-compose up -d` to create a instance of Postgres
- Go to prisma folder and create a .env file
- Add the `DATABASE_URL` variable with the url of your databse
- Run `npx prisma migrate dev --name init --preview-feature` to creates a new SQL migration file and runs the SQL migration file against the database

## How to use

- Run `yarn start:dev`
- Now you can connect with your front-end application running Apollo
- If you don't have a front-end application already, you could generate an `idToken` from the user created on the Firebase authentication by running a scrpit that connects with your Firebase project and calls `firebase.auth().signInWithEmailAndPassword(email, password)`)

## What it does?

- Create account
- Authenticate user
- List all pokemons with basic infos like weight, name, stats etc (like the Pokedex)
- Allows you to save the pokemons that you already catched
- List all only pokemons that you catched
- Delete catched pokemons
